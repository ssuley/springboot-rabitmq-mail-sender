package com.suleysoft.rabitmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabitmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabitmailApplication.class, args);
	}

}
